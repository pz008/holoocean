.. _`agents`:

HoloOcean Agents
=================

Documentation on specific agents available in HoloOcean:

.. toctree::
   :maxdepth: 1
   :caption: Agents

   hovering-auv-agent
   torpedo-auv-agent
   turtle-agent
   uav-agent
